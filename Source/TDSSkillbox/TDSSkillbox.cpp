// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSSkillbox.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDSSkillbox, "TDSSkillbox" );

DEFINE_LOG_CATEGORY(LogTDSSkillbox)
 