// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDSSkillboxGameMode.generated.h"

UCLASS(minimalapi)
class ATDSSkillboxGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDSSkillboxGameMode();
};



