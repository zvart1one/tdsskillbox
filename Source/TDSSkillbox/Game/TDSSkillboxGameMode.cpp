// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSSkillboxGameMode.h"
#include "TDSSkillboxPlayerController.h"
#include "TDSSkillbox/Character/TDSSkillboxCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATDSSkillboxGameMode::ATDSSkillboxGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDSSkillboxPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
